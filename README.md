# EM Glossary Project

Research data has to fulfil a number of fundamental requirements in order to make the most of it - today and in the future. The FAIR principles [1] – findable, accessible, interoperable, and reusable - describe such dispositions for digital data, but the pathway to realisation is often unclear. Here we focus on the challenge of implementing the ‘I’ in FAIR. Interoperability is the potential for two independent agents to work on the same data in a coordinated fashion.

Achieving interoperability of research data requires interoperable metadata. This raises problems for the researcher collecting metadata: How do I annotate data and document the way they have been generated so that other user understand what I mean? How do I disambiguate terms that are used in adjacent scientific disciplines but have differing meanings?

The use and adoption of established vocabularies and semantic standards can provide solutions – however these may be unfeasible to implement, non-existent, or scientifically unsuitable. These are some of the reasons why a number of initiatives are developing semantic artefacts that aim to describe experimental equipment, workflows, and analysis procedures in electron microscopies. Harmonisation of these efforts to ensure interoperability across the community is required.

To support the long-term semantic interoperability of these efforts the Helmholtz Metadata Collaboration (HMC) is coordinating a community-wide effort, including over 45 scientists from more than 22 institutions across Switzerland, Austria, and Germany including representatives of several NFDI consortia [2], to create a joint resource that will harmonize semantics in the field of electron and ion microscopies.

The EM glossary group strives to achieve consensus on terms commonly used in electron and ion microscopies via a remote, collaborative workflow based on the platform GitLab. With these we will establishe a ressource which provides harmonized and machine actionable semantics to support specific development efforts such as metadata schemas or ontologies in their respective fields. This will benefit experimental scientists by providing clarity when annotating and re-using research data; and by facilitating communication when conducting interdisciplinary studies.

Do you work in electron and/or ion microscopy or related data analysis? Follow us on twitter @helmholtz_hmc or tweet #EMGlossary. You want to get involved in the development and/or join our meetings? Send an email to hmc@fz-juelich.de or hmc-matter@helmholtz-berlin.de
References

Wilkinson, M.D. et al. (2016) Scientific Data. http://dx.doi.org/10.1038/sdata.2016.18

NFDI: Nationale Forschungsdateninfrastruktur. https://www.nfdi.de/

## Technologies Used

- Flask
- Next.js
- MongoDB
- Docker
- Docker Compose

## Prerequisites

- [Docker](https://www.docker.com/products/docker-desktop)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Installation

1. Clone the repository:

```bash
git clone https://codebase.helmholtz.cloud/em_glossary/em_glossary_web.git
```

2. Build and start the Docker containers:

```bash
docker-compose up --build
```

This command will build the Docker images and start the containers.

3. Access the Flask application at `http://localhost:5000` and the Next.js application at `http://localhost:3000`.

## Configuration

- Next.js:
  - Next.js application configuration can be updated in `next.config.js`.
- MongoDB:
  - MongoDB connection details can be updated in `docker-compose.yml`.

## Usage

- To start the Docker containers:

```bash
docker-compose up
```

- To stop the Docker containers:

```bash
docker-compose down
```

## Contributors

- Mojeeb Rahman Sedeqi
- Özlem Özkan
- Volker Hofmann
- Oonagh Mannix

More information can be find in the CITATION.cff of this repository.

## Support and Contributing

If you are interested in contributing to the project or have any questions not answered here or in the API documentation, please contact hmc@fz-juelich.de or hmc-matter@helmholtz-berlin.de.

## License

The `EM Glossary` are licensed under the Apache License, Version 2.0 (
the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License in the LICENSE in this repository or at

    http://www.apache.org/licenses/LICENSE-2.0

## Acknowledgements

- [Flask documentation](https://flask.palletsprojects.com/)
- [Next.js documentation](https://nextjs.org/docs)
- [MongoDB documentation](https://docs.mongodb.com/)
- [Docker documentation](https://docs.docker.com/)
- [Docker Compose documentation](https://docs.docker.com/compose/)
