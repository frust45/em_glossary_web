import AdopterItem from "@/components/AdopterItem";

const adopters_list = [
  {
    name: "FAIRmat",
    logo: "FAIRmat_new_with_text.png",
    url: "https://www.fairmat-nfdi.eu/fairmat",
    info: "FAIRmat is a consortium of the NFDI. We provide the condensed-matter physics and chemical physics of solids community with the infrastructure, tools, and knowledge to make their research data FAIR.",
  },
  {
    name: "MDMC",
    logo: "FAIRmat_new_with_text.png",
    url: "https://www.fairmat-nfdi.eu/fairmat",
    info: "FAIRmat is a consortium of the NFDI. We provide the condensed-matter physics and chemical physics of solids community with the infrastructure, tools, and knowledge to make their research data FAIR.",
  },
  {
    name: "Matwerk",
    logo: "FAIRmat_new_with_text.png",
    url: "https://www.fairmat-nfdi.eu/fairmat",
    info: "FAIRmat is a consortium of the NFDI. We provide the condensed-matter physics and chemical physics of solids community with the infrastructure, tools, and knowledge to make their research data FAIR.",
  },
];

export default async function Page() {
  return (
    <div className="basis-full ">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
        <h1>Adopters</h1>
      </div>
      {adopters_list &&
        adopters_list.map((item: any, index: number) => (
          <AdopterItem key={"adopter_" + index} item={item} />
        ))}
    </div>
  );
}
