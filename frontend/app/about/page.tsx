export default function About() {
  return (
    <div className="no-print container mx-auto">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
        <h1 className="text-2xl font-semibold">EM Glossary</h1>
      </div>
      <div
        className="m-2 flex flex-row flex-wrap  justify-between p-8
        sm:m-12  "
      >
        <p className="py-2 text-justify">
          Research data has to fulfil a number of fundamental requirements in
          order to make the most of it - today and in the future. The FAIR
          principles [1] – findable, accessible, interoperable, and reusable -
          describe such dispositions for digital data, but the pathway to
          realisation is often unclear. Here we focus on the challenge of
          implementing the ‘I’ in FAIR. Interoperability is the potential for
          two independent agents to work on the same data in a coordinated
          fashion.
        </p>
        <p className="py-2 text-justify">
          Achieving interoperability of research data requires interoperable
          metadata. This raises problems for the researcher collecting metadata:
          How do I annotate data and document the way they have been generated
          so that other user understand what I mean? How do I disambiguate terms
          that are used in adjacent scientific disciplines but have differing
          meanings?
        </p>

        <p className="py-2 text-justify">
          The use and adoption of established vocabularies and semantic
          standards can provide solutions – however these may be unfeasible to
          implement, non-existent, or scientifically unsuitable. These are some
          of the reasons why a number of initiatives are developing semantic
          artefacts that aim to describe experimental equipment, workflows, and
          analysis procedures in electron microscopies. Harmonisation of these
          efforts to ensure interoperability across the community is required.
        </p>
        <p className="py-2 text-justify">
          To support the long-term semantic interoperability of these efforts
          the Helmholtz Metadata Collaboration (HMC) is coordinating a
          community-wide effort, including over 45 scientists from more than 22
          institutions across Switzerland, Austria, and Germany including
          representatives of several NFDI consortia [2], to create a joint
          resource that will harmonize semantics in the field of electron and
          ion microscopies.
        </p>
        <p className="py-2 text-justify">
          The EM glossary group strives to achieve consensus on terms commonly
          used in electron and ion microscopies via a remote, collaborative
          workflow based on the platform GitLab. With these we will establishe a
          ressource which provides harmonized and machine actionable semantics
          to support specific development efforts such as metadata schemas or
          ontologies in their respective fields. This will benefit experimental
          scientists by providing clarity when annotating and re-using research
          data; and by facilitating communication when conducting
          interdisciplinary studies.
        </p>
      </div>
    </div>
  );
}
