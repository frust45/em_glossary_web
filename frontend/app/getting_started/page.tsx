export default function GettingStarted() {
  return (
    <div className="no-print container mx-auto">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
        <h1 className="text-2xl font-semibold">
          Getting Started with the EM Glossary
        </h1>
      </div>
      <div
        className="m-2 flex flex-col flex-wrap justify-between  gap-4 p-8
        sm:m-6  "
      >
        <p className="py-2 text-justify">
          The EM Glossary is a resource for researchers and professionals in
          electron and ion microscopy, designed to enhance the clarity and
          interoperability of terminology within these fields.
        </p>
        <h1 className="text-2xl font-semibold">How to Use the EM Glossary?</h1>
        <h2 className="text-xl font-semibold">Search Function</h2>
        <ul className="flex list-disc flex-col gap-2 px-10 text-justify">
          <li>
            <span className="font-semibold">
              Browse Alphabetically Click a Letter:
            </span>{" "}
            On the homepage, select any letter to see terms starting with it.
          </li>
          <li>
            <span className="font-semibold">
              Search Directly Type in the Search Box:
            </span>{" "}
            {`Use the search box on the homepage to enter the term you're looking
            for.`}
          </li>
          <li>
            <span className="font-semibold">Autocomplete Suggestions:</span> As
            you type, in the below you can find related terms.
          </li>
        </ul>

        <h2 className="text-xl font-semibold">TBD: </h2>
        <ul className="flex list-disc flex-col gap-2 px-10 text-justify text-red-500">
          <li>
            Citing Terms in Research Refer to the IRI section for a persistent
            identifier that facilitates the citation of glossary terms in your
            research documentation and publications.
          </li>
          <li>
            Contributing to the Glossary The glossary is a collaborative effort.
            If you have expertise in electron or ion microscopy and wish to
            contribute new terms or refine existing ones, please reach us via
            ....
          </li>
          <li>
            Assistance and Updates Should you require help or wish to provide
            feedback, contact the support team via ...
          </li>
        </ul>
        <h2 className="text-xl font-semibold">Glossary Term Attributes </h2>
        <ul className="flex list-disc flex-col gap-2 px-10 text-justify">
          <li>
            <span className="font-semibold">Label:</span>
            {`This attribute provides a human-readable version of a term's name, making
            it easily identifiable and understandable to the user.`}
          </li>
          <li>
            <span className="font-semibold">Definition:</span>{" "}
            {`It represents the meaning of the term in English, offering a clear
            and concise explanation to ensure the user's comprehensive
            understanding.`}
          </li>
          <li>
            <span className="font-semibold"> Comments:</span>{" "}
            {`This
            offers additional context or explanations regarding the term or its
            usage, enriching the user's knowledge with deeper insights.`}
          </li>
          <li>
            <span className="font-semibold"> Singular:</span> Denoting a unit
            quantity of the term, this indicates how the term is used when
            referring to a single instance.
          </li>
          <li>
            <span className="font-semibold"> Plural:</span> Representing more
            than one of the term, this specifies the plural form, guiding
            correct usage in varied contexts.
          </li>
          <li>
            <span className="font-semibold"> Acronyms:</span> An abbreviation
            formed from the initial letters of other words and pronounced as a
            word, providing a concise reference that may be commonly used in
            literature or discussions.
          </li>
          <li>
            <span className="font-semibold">Abbreviations:</span> This is a
            shortened form of words used to represent the whole, facilitating
            easier and quicker reference in texts.
          </li>
          <li>
            <span className="font-semibold">Exact Synonym:</span> An exact
            synonym is entirely interchangeable with the class label,
            corresponding to the same definition, and thus, offering alternative
            terminology without changing the meaning.
          </li>
          <li>
            <span className="font-semibold">Narrow Synonym:</span> A narrow
            synonym shares a similar meaning with another word but represents a
            more specific or limited concept, not covering all aspects of the
            broader term.
          </li>
          <li>
            <span className="font-semibold">Broad Synonym:</span> A broad
            synonym has a similar meaning to another word but represents a more
            general or inclusive concept, encompassing a wider range of meanings
            than the specific term.
          </li>
          <li>
            <span className="font-semibold">Related Synonym:</span> A related
            synonym overlaps in meaning with another word, but it may not be an
            exact match, indicating a looser relationship where the terms share
            similarities without being precisely synonymous.
          </li>
          <li>
            <span className="font-semibold">Sources:</span>{" "}
            {`This lists URLs to
            sources or references related to the entity, linking the user to
            further reading or evidence supporting the term's definition.`}
          </li>
          <li>
            <span className="font-semibold">IRI (iri):</span> Provides the
            Internationalized Resource Identifier, offering a direct link to
            more information or related resources for comprehensive
            understanding.
          </li>
          <li>
            <span className="font-semibold">Examples:</span> This attribute
            gives examples to illustrate the use or context of the entity,
            aiding in the practical application and deeper comprehension of the
            term.
          </li>
          <li>
            <span className="font-semibold"> Contributors:</span> Identifying
            the ORCID of individuals who contributed to discussions pertaining
            to any class label of a term, acknowledging their input and
            expertise.
          </li>
          <li>
            <span className="font-semibold">Ratified (ratified):</span>{" "}
            {`Indicates whether the entity's information has been officially
            approved or ratified, ensuring the reliability and accuracy of the
            information provided.`}
          </li>
        </ul>
      </div>
    </div>
  );
}
