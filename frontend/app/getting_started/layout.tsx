import FAQ from "@/components/FAQ";
import JoinWithUs from "@/components/JoinWithUs";

export default function AboutLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <div className="container mx-auto ">
        <div
          className="m-2 flex flex-col flex-wrap rounded-lg border 
        sm:m-12 sm:flex-row"
        >
          {children}
        </div>
      </div>
      <JoinWithUs />
      <FAQ />
    </div>
  );
}
