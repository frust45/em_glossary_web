import "./globals.css";
import "react-tooltip/dist/react-tooltip.css";
import type { Metadata } from "next";
import localFont from "next/font/local";
// import { Inter } from 'next/font/google'
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import ScrollToTop from "@/components/ScrollToTop";
import Banner from "@/components/Banner";

const halvar = localFont({
  src: [
    {
      path: "./fonts/HelmholtzHalvarMittel-Lt.woff2",
      weight: "300",
      style: "normal",
    },
    {
      path: "./fonts/HelmholtzHalvarMittel-Rg.woff2",
      weight: "400",
      style: "normal",
    },
    {
      path: "./fonts/HelmholtzHalvarMittel-Bd.woff2",
      weight: "700",
      style: "normal",
    },
    {
      path: "./fonts/HelmholtzHalvarMittel-LtSlanted.woff2",
      weight: "300",
      style: "italic",
    },
    {
      path: "./fonts/HelmholtzHalvarMittel-RgSlanted.woff2",
      weight: "400",
      style: "italic",
    },
    {
      path: "./fonts/HelmholtzHalvarMittel-BdSlanted.woff2",
      weight: "700",
      style: "italic",
    },
  ],
});

// const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: "HMC Glossary",
  description: "HMC Glossary System, EM Glossary",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={halvar.className}>
        <Header />
        <Banner />
        {children}
        <Footer />
        <ScrollToTop />
      </body>
    </html>
  );
}
