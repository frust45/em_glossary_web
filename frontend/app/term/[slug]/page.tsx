import { get_cross_ref, ourDecodedURIStr } from "@/helper/helper";
import NoTermsFound from "@/components/TermListItem/NoTermsFound";

import TermBar from "@/components/TermListItem/TermBar";
import PrintButton from "@/components/PrintButton";
import Label from "@/components/TermDetails/Label";
import Definition from "@/components/TermDetails/Definition";
import InternalCrossReference from "@/components/TermDetails/InternalCrossReference";
import Contributors from "@/components/TermDetails/Contributors";
import SeeAlso from "@/components/TermDetails/SeeAlso";
import Sources from "@/components/TermDetails/Sources";
import Examples from "@/components/TermDetails/Examples";
import Synonyms from "@/components/TermDetails/Synonyms";
import Abbreviations from "@/components/TermDetails/Abbreviations";
import Acronyms from "@/components/TermDetails/Acronyms";
import Plural from "@/components/TermDetails/Plural";
import Singular from "@/components/TermDetails/Singular";
import Comments from "@/components/TermDetails/Comments";
import Iri from "@/components/TermDetails/Iri";
import { Suspense } from "react";
import Loading from "../loading";

export const dynamic = "force-dynamic";

export const revalidate = 0;

async function getData(slug: string) {
  const res = await fetch(process.env.BACKEND_URL + "api/term/" + slug);
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary

    console.log(res);
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page({ params }: { params: { slug: string } }) {
  const { slug } = params;
  const data = await getData(slug);

  const {
    label,
    singular,
    plural,
    acronyms,
    abbreviations,
    synonyms,
    definition,
    comments,
    iri,
    seeAlso,
    contributors,
    examples,
    sources,
    hmc_cross_ref,
  } = data.result;

  const list_ref = get_cross_ref(hmc_cross_ref);

  return (
    <div className="print-only basis-full sm:basis-3/4">
      <Suspense fallback={<Loading />}>
        <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
          <h1>Term: {ourDecodedURIStr(String(label).toUpperCase())}</h1>
        </div>

        {data && data.result.url_slug ? (
          <>
            <div className="flex flex-auto flex-col p-4 text-xs leading-normal text-primary  sm:text-base ">
              <div className="grid gap-2 sm:gap-6">
                {/*  */}
                <Label label={label} />
                <Definition
                  definition={definition}
                  hmc_cross_ref={hmc_cross_ref}
                />
                <InternalCrossReference
                  list_ref={list_ref}
                  hmc_cross_ref={hmc_cross_ref}
                />
                <Comments comments={comments} />
                <Singular singular={singular} />

                <div className="col-span-4">
                  <hr className="mx-auto my-4 h-1 w-48 rounded border-0 bg-primary md:my-10" />
                </div>

                <Plural plural={plural} />
                <Acronyms acronyms={acronyms} />
                <Abbreviations abbreviations={abbreviations} />
                <Synonyms synonyms={synonyms} />
                <Iri iri={iri} />
                <Examples examples={examples} />
                <Sources sources={sources} />
                <SeeAlso seeAlso={seeAlso} />
                <Contributors contributors={contributors} />
              </div>

              <div className="flex justify-between p-4 pt-8">
                <PrintButton />
                <TermBar term={data.result} />
              </div>
            </div>
          </>
        ) : (
          <NoTermsFound />
        )}
      </Suspense>
    </div>
  );
}
