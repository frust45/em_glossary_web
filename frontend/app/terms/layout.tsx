import SearchBar from "@/components/SearchBar";
import Sidebar from "@/components/Sidebar";
import JoinWithUs from "@/components/JoinWithUs";
import GettingStarted from "@/components/GettingStarted";
import { Suspense } from "react";
import Loading from "./loading";

export default function TermsLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary">
      <GettingStarted />
      <SearchBar />
      <div className="container mx-auto ">
        <div
          className="m-2 flex flex-col flex-wrap rounded-lg border 
        sm:m-12 sm:flex-row"
        >
          <Sidebar />
          {children}
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
