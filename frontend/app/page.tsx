import SearchBar from "@/components/SearchBar";
import Sidebar from "@/components/Sidebar";
import GettingStarted from "@/components/GettingStarted";
import Statistics from "@/components/Statistics";
import WordOfTheWeek from "@/components/WordOfTheWeek";
import JoinWithUs from "@/components/JoinWithUs";

export default function Home() {
  return (
    <div className="min-h-full text-primary">
      <GettingStarted />
      <SearchBar />
      <div className="container mx-auto ">
        <div
          className="m-2 flex flex-col flex-wrap rounded-lg border 
        sm:m-12 sm:flex-row"
        >
          <Sidebar />

          <div className="basis-full sm:basis-3/4">
            <WordOfTheWeek />
            <Statistics />
          </div>
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
