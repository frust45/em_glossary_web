import json
import os
from datetime import datetime, timedelta, timezone

from bson import json_util
from dotenv import load_dotenv, find_dotenv
from flask import Flask, jsonify, request
from flask_cors import CORS
from pymongo import MongoClient, DESCENDING

load_dotenv(find_dotenv())

# app instance
app = Flask(__name__)
CORS(app)

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")

SOURCE = [
    {
        "id": 1,
        "name": "Electron and/or Ion Microscopy Glossary",
        "slug_name": "em",
        "source": "Gitlab Source",
        "url": "https://codebase.helmholtz.cloud/em_glossary/em_glossary",
    }
]


def get_terms_collection():
    client = MongoClient(DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["terms"]


def get_all_terms():
    collection = get_terms_collection()
    page = request.args.get("page", 1, type=int)
    per_page = request.args.get("per_page", 20, type=int)
    offset = (page - 1) * per_page
    total_documents = collection.count_documents({})
    result = collection.find().sort("label").skip(offset).limit(per_page)

    data = {
        "page": page,
        "per_page": per_page,
        "total_documents": total_documents,
        "result": list(result),
    }
    return data


def get_terms_by_list_slug(slug):
    collection = get_terms_collection()
    page = request.args.get("page", 1, type=int)
    per_page = request.args.get("per_page", 20, type=int)
    offset = (page - 1) * per_page
    total_documents = collection.count_documents({"label": {"$regex": "^" + slug}})
    result = (
        collection.find({"label": {"$regex": "^" + slug}})
        .sort("label")
        .skip(offset)
        .limit(per_page)
    )
    data = {
        "page": page,
        "per_page": per_page,
        "total_documents": total_documents,
        "result": list(result),
    }
    return data


def get_term_by_slug(slug):
    collection = get_terms_collection()
    result = collection.find_one({"url_slug": slug})
    collection.update_one({"url_slug": slug}, {"$inc": {"view_count": 1}})
    data = {"result": result}
    return data


def get_terms_by_query(query):
    collection = get_terms_collection()
    keywords = query.split()
    query = {"label": {"$regex": ".*?" + keywords[0] + ".*?"}}
    for keyword in keywords[1:]:
        query["label"]["$regex"] += ".*?" + keyword + ".*?"
    result = collection.find(query).sort("label")
    data = {"result": list(result)}
    return data


def get_terms_and_source_statistics():
    collection = get_terms_collection()
    total_documents = collection.count_documents({})
    pipeline = [
        {
            "$group": {
                "_id": None,
                "total_view": {"$sum": "$view_count"},
                "min_date": {"$min": "$hmc_glossary_metadata.source_update_datetime"},
                "max_date": {"$max": "$hmc_glossary_metadata.source_update_datetime"},
            }
        }
    ]

    aggregate_result = list(collection.aggregate(pipeline))
    min_date = aggregate_result[0]["min_date"].strftime("%Y-%m-%d")
    max_date = aggregate_result[0]["max_date"].strftime("%Y-%m-%d")
    total_views = aggregate_result[0]["total_view"]

    data = {
        "total_terms": total_documents,
        "total_views": total_views,
        "min_date": min_date,
        "max_date": max_date,
        "source": SOURCE,
    }
    return data


###############################################
def get_contributors_collection():
    client = MongoClient(DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["contributors"]


def get_contributors_list():
    collection = get_contributors_collection()
    result = collection.find({"staff_type": "scientist_staff"}).sort(
        "count_terms_contribute", DESCENDING
    )
    data = {
        "result": list(result),
    }
    return data


def get_contributors_list_draft_1():
    collection = get_contributors_collection()
    page = request.args.get("page", 1, type=int)
    per_page = request.args.get("per_page", 20, type=int)
    offset = (page - 1) * per_page
    total_documents = collection.count_documents({"staff_type": "scientist_staff"})
    result = (
        collection.find({"staff_type": "scientist_staff"})
        .sort("count_terms_contribute", DESCENDING)
        .skip(offset)
        .limit(per_page)
    )

    data = {
        "page": page,
        "per_page": per_page,
        "total_documents": total_documents,
        "result": list(result),
    }
    return data


N_WEEK = 10
UPDATE_ALLOW = True
FILTER_TERM_LIST = [
    "electron beam",
    "electron diffraction",
    "electron diffraction pattern",
    "ion beam",
    "time period",
]


def get_word_of_the_week_collection():
    client = MongoClient(DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["word_of_the_week"]


def get_word_of_the_week_list():
    collection = get_word_of_the_week_collection()
    result = collection.find()

    data = {
        "result": list(result),
    }
    return data


def get_current_word_of_the_week():
    collection = get_word_of_the_week_collection()
    result = collection.find().sort("date", DESCENDING).limit(1)[0]
    term = get_terms_collection().find_one({"_id": result["term_id"]})

    data = {
        "result": term,
    }
    return data


def save_new_word_of_the_week(term_id):
    collection = get_word_of_the_week_collection()
    result = {}

    term = get_terms_collection().find_one({"_id": term_id})
    # pprint.pprint(term)
    word_of_the_week = {
        "term_id": term_id,
        "label": term["label"],
        "definition": term["definition"],
        "url_slug": term["url_slug"],
        "date": datetime.now(tz=timezone.utc),
    }

    inserted_word_id = collection.insert_one(word_of_the_week).inserted_id
    if inserted_word_id:
        result = collection.find_one(inserted_word_id)

    return result


def _update_word_of_the_week(term_id):
    collection = get_word_of_the_week_collection()
    last_word_of_the_week = collection.find({}).sort("date", DESCENDING).limit(1)[0]
    term = get_terms_collection().find_one({"_id": term_id})
    print(
        f"Last word of the week was: ({last_word_of_the_week['label']}), "
        f"and update to: ({term['label']})"
    )
    word_of_the_week = {
        "term_id": term_id,
        "label": term["label"],
        "definition": term["definition"],
        "url_slug": term["url_slug"],
        "date": datetime.now(tz=timezone.utc),
    }

    return collection.update_one(
        {"_id": last_word_of_the_week["_id"]}, {"$set": word_of_the_week}
    )


def _select_random_term():
    pipeline = [
        {"$sample": {"size": 1}},
    ]
    selected_term = list(get_terms_collection().aggregate(pipeline))[0]
    # pprint.pprint(selected_term)
    return selected_term


def _is_word_exist_for_current_week():
    today = datetime.now()
    last_week = today - timedelta(days=7)

    word_count = get_word_of_the_week_collection().count_documents(
        {"date": {"$gte": last_week}}
    )
    # print(word_count)
    if word_count > 0:
        return True
    else:
        return False


def _count_word_of_the_week_in_n_week(label):
    today = datetime.now()
    date_n_week = today - timedelta(weeks=10)

    word_count = get_word_of_the_week_collection().count_documents(
        {"label": label, "date": {"$gte": date_n_week}}
    )

    return word_count


def _generate_new_word_of_the_week(update):
    selected_term = _select_random_term()
    label = selected_term["label"]
    term_id = selected_term["_id"]
    print(f"The suggested random term: ({label})")
    if label in FILTER_TERM_LIST:
        print("The term is in the filtered list.")
        _generate_new_word_of_the_week(update)
    else:
        print("The term is not in the filtered list.")
        print(
            "Now we will go for next step to check this term should not be in N-LAST word of the week!"
        )

        count_word = _count_word_of_the_week_in_n_week(label)
        print(f"Last N week count: {count_word}")

        if count_word > 0:
            print(f"The {label} term exit on last {N_WEEK} week!")
            _generate_new_word_of_the_week(update)
        else:
            print(f"The {label} term NOT exit on last {N_WEEK} week!")
            if update:
                data = _update_word_of_the_week(term_id)
                print(f"Successfully UPDATE the word of the week")
            else:
                data = save_new_word_of_the_week(term_id)
                print(f"Successfully SAVE the word of the week")
            return data


def generate_new_word_of_the_week():
    word_exit = _is_word_exist_for_current_week()

    if word_exit:
        print(f"Word for the current week exist status {word_exit}")
        print("Word of the week already exist!")

        if UPDATE_ALLOW:
            print("Update option is ON!, so we will update, if word already exist!")
            return _generate_new_word_of_the_week(True)
        print(
            "Update option is OFF!, just exit without effecting the current word of the week!"
        )
        return None
    else:
        print("Word of the week do not exist!, so create it!")
        return _generate_new_word_of_the_week(False)


###############################################


# /api/home
@app.route("/api/home", methods=["GET"])
def return_home():
    return jsonify(
        {
            "message": "Welcome",
            "people": ["Mojeeb", "Özlem", "Volker", "Oonagh"],
        }
    )


# /api/terms
@app.route("/api/terms", methods=["GET"])
def return_terms():
    return json.dumps(get_all_terms(), default=json_util.default)


@app.route("/api/terms/<slug>", methods=["GET"])
def return_terms_list(slug):
    return json.dumps(get_terms_by_list_slug(slug), default=json_util.default)


@app.route("/api/term/<slug>", methods=["GET"])
def return_term(slug):
    return json.dumps(get_term_by_slug(slug), default=json_util.default)


@app.route("/api/search_terms/<query>", methods=["GET"])
def return_search_terms(query):
    return json.dumps(get_terms_by_query(query), default=json_util.default)


@app.route("/api/statistics", methods=["GET"])
def return_statistics():
    return json.dumps(get_terms_and_source_statistics(), default=json_util.default)


@app.route("/api/contributors", methods=["GET"])
def return_contributors():
    return json.dumps(get_contributors_list(), default=json_util.default)


@app.route("/api/current_word_of_the_week", methods=["GET"])
def return_current_word_of_the_week():
    return json.dumps(get_current_word_of_the_week(), default=json_util.default)
