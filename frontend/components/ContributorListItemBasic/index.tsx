import Image from "next/image";
const TermListItem = ({ contributor }: { contributor: any }) => {
  return (
    <>
      {contributor && (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`${contributor.orcid}`}
          className="m-1 flex  flex-row flex-wrap justify-between   rounded-lg border bg-white shadow-sm hover:bg-gray-100
           md:flex-row"
        >
          <h2 className="break-all p-2 text-lg font-bold  text-gray-900  ">
            <span
              className="bg-gradient-to-r from-primary to-info bg-clip-text text-transparent 
               "
            >
              {contributor.name}
            </span>
          </h2>

          <div className="flex flex-row flex-wrap pb-2">
            <p className="px-2 ">
              <span className="font-normal px-2  text-lime-500 ">
                {contributor.center}
              </span>
              <span> - </span>
              <Image
                src="/images/orcid.svg"
                alt="pch.vector / Freepik"
                width={30}
                height={30}
                className="w-3 mr-1 inline-flex"
              />
              <span className="text-xs">{contributor.orcid}</span>
            </p>
            <p className=" px-2">
              <span className="text-xs font-normal px-2  ">
                Terms Contribution Count:{" "}
                <span className="font-bold text-lime-500">
                  {contributor.count_terms_contribute}
                </span>
              </span>
            </p>
          </div>
        </a>
      )}
    </>
  );
};

export default TermListItem;
