import Link from "next/link";
import { toSentenceCase } from "../../helper/helper";
import TermBar from "./TermBar";
import { Tooltip } from "react-tooltip";
const WordOfTheWeekItem = ({ term }: { term: any }) => {
  return (
    <>
      {term && (
        <Link
          href={`/term/${term.url_slug}`}
          className="tooltip_word_of_the_week m-6 flex  flex-col justify-start 
            rounded-lg border   bg-primary    text-white
                   shadow-sm  hover:bg-slate-100
                   hover:text-primary
                    group-hover:opacity-90 md:flex-row 
                    "
        >
          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
              <span>Word of the week</span>
            </h2>
            <hr className="mx-auto my-1 h-1 w-48 rounded border-0 bg-gray-200 md:my-1 "></hr>
            <h2 className="break-all p-2 text-xl font-bold   ">
              <span
                className="bg-gradient-to-r  from-sky-600 to-cyan-100 bg-clip-text text-transparent 
               "
              >
                {toSentenceCase(term.label)}
              </span>
            </h2>

            <div className="flex flex-col flex-wrap">
              <h5 className="px-2 font-normal ">{term.definition}</h5>
            </div>
            <div className="flex justify-between">
              <p className="pt-4 text-xs">
                Click to see full details of the term!
              </p>
              <div className="flex justify-end">
                <TermBar term={term} />
              </div>
            </div>
          </div>
          {/* <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_word_of_the_week`}
            place="bottom"
            content={`Word of the week: Click to see full details of the term!`}
          /> */}
        </Link>
      )}
    </>
  );
};

export default WordOfTheWeekItem;
