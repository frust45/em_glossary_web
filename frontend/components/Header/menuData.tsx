import { Menu } from "@/types/menu";

const menuData: Menu[] = [
  {
    id: 1,
    title: "Home",
    path: "/",
    newTab: false,
  },
  {
    id: 2,
    title: "Getting Started",
    path: "/getting_started",
    newTab: false,
  },
  {
    id: 3,
    title: "Terms",
    path: "/terms",
    newTab: false,
  },

  {
    id: 4,
    title: "Contributors",
    path: "/contributors",
    newTab: false,
  },

  {
    id: 5,
    title: "Adopters",
    path: "/adopters",
    newTab: false,
  },
  // {
  //   id: 3,
  //   title: "Contributors",
  //   newTab: false,
  //   submenu: [
  //     {
  //       id: 41,
  //       title: "Contributors Design 1 (Main)",
  //       path: "/contributors",
  //       newTab: false,
  //     },
  //     {
  //       id: 42,
  //       title: "Contributors Design 2 (Draft)",
  //       path: "/contributors_draft_1",
  //       newTab: false,
  //     },
  // {
  //   id: 43,
  //   title: "Blog Grid Page",
  //   path: "/blog",
  //   newTab: false,
  // },
  // {
  //   id: 44,
  //   title: "Blog Sidebar Page",
  //   path: "/blog-sidebar",
  //   newTab: false,
  // },
  // {
  //   id: 45,
  //   title: "Blog Details Page",
  //   path: "/blog-details",
  //   newTab: false,
  // },
  // {
  //   id: 46,
  //   title: "Sign In Page",
  //   path: "/signin",
  //   newTab: false,
  // },
  // {
  //   id: 47,
  //   title: "Sign Up Page",
  //   path: "/signup",
  //   newTab: false,
  // },
  // {
  //   id: 48,
  //   title: "Error Page",
  //   path: "/error",
  //   newTab: false,
  // },
  //   ],
  // },
  {
    id: 6,
    title: "About",
    path: "/about",
    newTab: false,
  },
];
export default menuData;
