"use client";

import React, { useEffect, useState } from "react";
import axios from "axios";
import { ColorRing } from "react-loader-spinner";

import WordOfTheWeekItem from "../WordOfTheWeekItem";

const WordOfTheWeek = () => {
  const [data, setData] = useState<any>({});
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(process.env.NEXT_PUBLIC_BACKEND_URL + "api/current_word_of_the_week")
      .then((data) => {
        // console.log(data.data);
        setData(data.data);
        setLoading(false);
      });
  }, []);

  return (
    <>
      {isLoading && (
        <ColorRing
          visible={true}
          height="80"
          width="80"
          ariaLabel="blocks-loading"
          wrapperStyle={{}}
          wrapperClass="blocks-wrapper"
          colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]}
        />
      )}
      {data && data.result && (
        <div className="no-print container mx-auto">
          <div className="mx-auto px-2 py-2 sm:max-w-xl md:max-w-full md:px-2 lg:max-w-screen-xl lg:px-2 lg:py-2">
            <WordOfTheWeekItem term={data.result} />
          </div>
        </div>
      )}
    </>
  );
};

export default WordOfTheWeek;
