import Image from "next/image";

const Footer = () => {
  return (
    <>
      <footer
        className="wow fadeInUp no-print relative z-10 bg-primary pt-10"
        data-wow-delay=".1s"
      >
        <div className="mx-12 flex flex-row justify-between ">
          <div className="mb-8 inline-block">
            <a
              href={`https://helmholtz-metadaten.de/en`}
              rel="nofollow noopener"
            >
              <Image
                src="/images/helmholtz_logo.svg"
                alt="logo"
                width={80}
                height={30}
                className="w-60 p-4 "
              />
            </a>
            <div className="mt-8 flex px-4">
              <a
                href={`https://www.linkedin.com/company/helmholtz-metadata-collaboration-hmc`}
                rel="nofollow noopener"
                target="_blank"
              >
                <Image
                  src="/images/icons/linkedin.svg"
                  alt="logo"
                  width={30}
                  height={30}
                  className="w-10 p-1 "
                />
              </a>
              <a
                href={`https://twitter.com/helmholtz_hmc`}
                rel="nofollow noopener"
                target="_blank"
              >
                <Image
                  src="/images/icons/x.svg"
                  alt="logo"
                  width={30}
                  height={30}
                  className="w-10 p-1 "
                />
              </a>
              <a
                href={`https://mattermost.hzdr.de/hmc-public`}
                rel="nofollow noopener"
                target="_blank"
              >
                <Image
                  src="/images/icons/mattermost.svg"
                  alt="logo"
                  width={30}
                  height={30}
                  className="w-10 p-1 "
                />
              </a>
              <a
                href={`https://helmholtz.social/@helmholtz_hmc`}
                rel="nofollow noopener"
                target="_blank"
              >
                <Image
                  src="/images/icons/mastodon.svg"
                  alt="logo"
                  width={30}
                  height={30}
                  className="w-10 p-1"
                />
              </a>
            </div>
          </div>
          <p className="mb-9 text-base font-medium leading-relaxed text-body-color">
            Better science through better metadata.
          </p>
        </div>

        <div className="flex w-full flex-row justify-between px-16 pb-12">
          <ul className="flex flex-row flex-wrap">
            <li>
              <a
                target="_blank"
                href="https://www.fz-juelich.de/en/privacy-policy"
                className="mx-4 inline-block text-base font-medium text-body-color hover:text-secondary"
              >
                Privacy protection
              </a>
            </li>
            <li>
              <a
                href="https://www.fz-juelich.de/en/legal-notice"
                target="_blank"
                className="mx-4 inline-block text-base font-medium text-body-color hover:text-secondary"
              >
                Legal information
              </a>
            </li>
            <li>
              <a
                target="_blank"
                href="https://www.fz-juelich.de/en/declaration-of-accessibility"
                className="mx-4 inline-block text-base font-medium text-body-color hover:text-secondary"
              >
                Accessibility
              </a>
            </li>
          </ul>
          <span className="text-center text-body-color  hover:text-secondary">
            ©
            <a
              target="_blank"
              href="https://helmholtz-metadaten.de/en"
              rel="nofollow noopener"
              className="px-2"
            >
              Helmholtz Metadata Collaboration
            </a>
          </span>
        </div>
      </footer>
    </>
  );
};

export default Footer;
