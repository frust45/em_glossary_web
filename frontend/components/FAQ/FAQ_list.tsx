"use client";

import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/20/solid";
import Markdown from "react-markdown";

const markdown = "# Hi, *Pluto*!";

const faq_text: any = [
  {
    title: "What is the EM Glossary?",
    body: `The EM Glossary is a comprehensive resource developed by the Helmholtz Metadata Collaboration. 
It aims to harmonize semantics in electron and ion microscopy research
 by providing clear definitions and explanations of key terms and concepts used in the field.`,
  },
  {
    title: "Who can benefit from using the EM Glossary?",
    body: `Researchers, students, educators, and professionals in the fields of electron and ion microscopy, as well as interdisciplinary areas involving microscopy, can benefit from the EM Glossary. Other glossaries and ontologies may benefit from the glossary. You can learn about different ways of using the glossary in our adopters page.`,
  },
  {
    title: "How can I search for terms in the EM Glossary?",
    body: `To search for terms, use the search bar located at the top of the page or use the letter buttons on the left side. You can enter a specific term or a related keyword to find definitions and explanations.`,
  },
  {
    title: "Can I contribute to the EM Glossary or suggest new terms?",
    body: `Yes! We would love for you to contribute to the EM Glossary. We meet regularly on Mondays at 1 pm (CEST, UTC +02:00). If you would be interested in receiving regular updates and meeting invitations please join our mailing list by sending an email to [hmc-matter@helmholtz-berlin.de](mailto:hmc-matter@helmholtz-berlin.de) with the subject “EM Glossary Signup”`,
  },
  {
    title: "Is there any cost to access the EM Glossary?",
    body: `The EM Glossary is available for free to all users. It is an open resource designed to support the microscopy community by providing easy access to standardized terminology. If you use the glossary please cite us (see the Getting Started page)`,
  },
  {
    title: "How is the information in the EM Glossary verified?",
    body: `?~~ `,
  },
  {
    title: "Are there any resources linked to the terms in the glossary?",
    body: `Yes, some terms in the glossary may include links to external resources, publications, or further readings that provide additional context or detailed explanations.`,
  },
  {
    title: "How often is the EM Glossary updated?",
    body: `The EM Glossary is a living work in progress. Updates are not on a fixed schedule; but are based on our contributors reaching a consensus on each definition. You can view our terms in active development here: [https://codebase.helmholtz.cloud/em_glossary/em_glossary](https://codebase.helmholtz.cloud/em_glossary/em_glossary)`,
  },
  {
    title:
      "Can I use the information from the EM Glossary in my research or publications?",
    body: `Yes, you are strongly encouraged to use the EM Glossary! Please cite the glossary accordingly, this shows our contributors the impact and value of their work.`,
  },
  {
    title:
      "Who maintains the EM Glossary, and how can I contact them for more information?",
    body: `The EM Glossary is maintained by the Helmholtz Metadata Collaboration.For more information or specific inquiries please contact hmc - [matter@helmholtz-berlin.de](mailto:matter@helmholtz-berlin.de).`,
  },
];

export default function FAQ_list() {
  return (
    <>
      {faq_text.map((item: any, index: number) => (
        <Disclosure key={"faq_" + index} as="div" className="mt-1">
          {({ open }) => (
            <>
              <Disclosure.Button
                className="flex w-full justify-between rounded-lg
               bg-info/[.2] px-4 py-2 text-left text-base
               font-medium  hover:bg-info/[.4]
                focus:outline-none focus-visible:ring
                 focus-visible:ring-info 
                 focus-visible:ring-opacity-75 "
              >
                <span>
                  <Markdown>{item.title}</Markdown>
                </span>
                <ChevronUpIcon
                  className={`${
                    open ? "rotate-180 transform" : ""
                  } h-5 w-5 bg-info/[.2]`}
                />
              </Disclosure.Button>
              <Disclosure.Panel className="px-4 pb-2 pt-4 text-sm">
                <Markdown>{item.body}</Markdown>
              </Disclosure.Panel>
            </>
          )}
        </Disclosure>
      ))}
    </>
  );
}
