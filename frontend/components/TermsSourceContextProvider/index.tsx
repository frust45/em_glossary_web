// "use client";
//
// import React, { createContext, useContext, useState } from "react";
//
// const TermsSourceContext = createContext([]);
//
// export const useTermsSourceContext = () => useContext(TermsSourceContext);
//
// const sourceList = [
//   {
//     id: 1,
//     name: "Electron and/or Ion Microscopy Glossary",
//     slug_name: "em",
//     source: "Gitlab Source",
//     url: "https://codebase.helmholtz.cloud/em_glossary/em_glossary",
//     active: true,
//   },
// ];
//
// const TermsSourceContextProvider = ({ children }: any) => {
//   const [termsSources, setTermsSources] = useState(sourceList);
//
//   const changeSourceStatus = (slug_name: string, e: boolean) => {
//     const newSource = termsSources.map((source) => {
//       if (slug_name === source.slug_name) {
//         return { ...source, active: e };
//       }
//       return source;
//     });
//     setTermsSources(newSource);
//   };
//
//   return (
//     <TermsSourceContext.Provider
//       value={{
//         termsSources,
//         setTermsSources,
//         changeSourceStatus,
//       }}
//     >
//       {children}
//     </TermsSourceContext.Provider>
//   );
// };
//
// export default TermsSourceContextProvider;
