"use client";

import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const PrintButton = () => {
  return (
    <>
      <button
        className="tooltip_print rounded bg-primary px-4 py-2 font-bold text-white hover:bg-info print:hidden"
        onClick={() => window.print()}
      >
        Print
      </button>
      <Tooltip
        className="my_tooltip"
        anchorSelect={`.tooltip_print`}
        place="bottom"
        content={tooltip_text["print"]}
      />
    </>
  );
};

export default PrintButton;
