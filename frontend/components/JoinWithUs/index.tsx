import Markdown from "react-markdown";

const JoinWithUs = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div className="">
          <span
            className="m-6  
               flex flex-col
         justify-start  rounded-lg
        border
      bg-info/[.2] 
        p-2 text-primary shadow-sm hover:bg-slate-100 
        hover:text-primary group-hover:opacity-90 sm:mx-12 md:flex-row"
          >
            <div className="flex flex-auto flex-col  p-4 leading-normal ">
              <h2 className="text- break-all p-2 text-2xl font-bold">
                <span>Join with us!</span>
              </h2>
              <div className="flex flex-col flex-wrap text-justify">
                <Markdown>{`All these definitions are the product of volunteers [(see
                  contributors page)](/contributors). We would love for you to join in and
                  contribute to the EM Glossary. We meet regularly on Mondays at
                  1 pm (CEST, UTC +02:00). If you would be interested in
                  receiving regular updates and meeting invitations please join
                  our mailing list by sending an email to [hmc-matter@helmholtz-berlin.de](mailto:hmc-matter@helmholtz-berlin.de)
                   or [hmc@fz-juelich.de](mailto:hmc@fz-juelich.de) with
                  the subject “EM Glossary Signup”`}</Markdown>
              </div>
            </div>
          </span>
        </div>
      </div>
    </>
  );
};

export default JoinWithUs;
