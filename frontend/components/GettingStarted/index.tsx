import Link from "next/link";

const GettingStarted = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div className="">
          <Link
            href={`/getting_started`}
            className="m-6  
               flex flex-col
         justify-start  rounded-lg
        border
      bg-info/[.2] 
        p-2 text-primary shadow-sm hover:bg-slate-100 
        hover:text-primary group-hover:opacity-90 sm:mx-12 md:flex-row"
          >
            <div className="flex flex-auto flex-col  p-4 leading-normal ">
              <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
                <span>Getting Started</span>
              </h2>
              <hr className="mx-auto my-2 h-2 w-48 rounded border-0 bg-gray-200 md:my-3 "></hr>

              <div className="flex flex-col flex-wrap text-center">
                <p>
                  {`New to the glossary? Need help in understanding how to use the
                  terms? We've got everything you need to get started here.`}
                </p>
              </div>
            </div>
          </Link>
        </div>
      </div>
    </>
  );
};

export default GettingStarted;
