import Image from "next/image";
const Banner = () => {
  return (
    <>
      {/* <div className="h-64 mt-14 bg-scroll bg-[url('/images/Banner.png')] no-print"> */}
      <div className="no-print mt-14 flex h-64 flex-row   flex-wrap content-center items-stretch justify-around bg-primary bg-scroll">
        <div>
          <h1 className="pt-8 text-center text-3xl font-semibold text-body-color">
            Electron & Ion Microscopy Glossary
          </h1>
        </div>
        <div>
          {" "}
          <Image
            src="/images/glossary_log.png"
            alt="logo"
            width={140}
            height={30}
            className="h-auto w-52 "
          />
        </div>
      </div>
    </>
  );
};

export default Banner;
