import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const LoadingSkeleton = () => {
  return (
    <div className="flex ">
      <Skeleton />
      <Skeleton count={5} />
    </div>
  );
};

export default LoadingSkeleton;
