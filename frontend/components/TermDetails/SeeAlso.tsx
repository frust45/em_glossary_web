"use client";

import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const SeeAlso = ({ seeAlso }: { seeAlso: any }) => {
  return (
    <>
      {seeAlso && (
        <>
          <div className="tooltip_see_also col-span-1 self-center">
            <h2 className="px-3 font-bold">See Also:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(seeAlso).map(([key, value]: any) => (
              <p key={`seeAlso${key}`} className="truncate px-2 text-justify">
                <a href={value} target="_blank" rel="noopener noreferrer">
                  {value}
                </a>
              </p>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_see_also`}
            place="bottom"
            content={"See Also"}
          />
        </>
      )}
    </>
  );
};

export default SeeAlso;
