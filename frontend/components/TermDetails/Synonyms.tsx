"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Synonyms = ({ synonyms }: { synonyms: any }) => {
  return (
    <>
      {synonyms && (
        <>
          <div className="tooltip_synonyms col-span-1 self-center">
            <h2 className="px-3 font-bold">Synonyms:</h2>
          </div>
          <div className="tooltip_synonyms_detail col-span-3">
            {Object.entries(synonyms).map(([key, value]: [string, any]) => (
              <span key={`synonyms${key}`}>
                <h5 className="font-bold">
                  {toSentenceCase(key)}: {toSentenceCase(value)}
                </h5>
                <Tooltip
                  className="my_tooltip"
                  anchorSelect={`.tooltip_synonyms_detail`}
                  place="bottom"
                  content={tooltip_text[key]}
                />
              </span>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_synonyms`}
            place="bottom"
            content={`Synonym corresponds to the same definition.`}
          />
        </>
      )}
    </>
  );
};

export default Synonyms;
