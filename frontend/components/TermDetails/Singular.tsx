"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Singular = ({ singular }: { singular: any }) => {
  return (
    <>
      {singular && (
        <>
          <div className="tooltip_singular col-span-1 self-center">
            <h2 className="px-3 font-bold">Singular:</h2>
          </div>
          <div className="col-span-3">
            <h2 className="break-all text-lg font-bold ">
              {toSentenceCase(singular.singular_en)}
            </h2>
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_singular`}
            place="bottom"
            content={tooltip_text["singular_en"]}
          />
        </>
      )}
    </>
  );
};

export default Singular;
