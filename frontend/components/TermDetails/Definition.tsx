"use client";

import { add_cross_ref_popover, toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Definition = ({
  definition,
  hmc_cross_ref,
}: {
  definition: any;
  hmc_cross_ref: any;
}) => {
  return (
    <>
      {definition && (
        <>
          <div className="tooltip_definitions col-span-1 self-center">
            <h2 className="px-3 font-bold">Definition:</h2>
          </div>
          <div className="col-span-3">
            <p
              className="text-base font-bold"
              dangerouslySetInnerHTML={{
                __html: add_cross_ref_popover(definition, hmc_cross_ref),
              }}
            ></p>
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_definitions`}
            place="bottom"
            content={tooltip_text["definition"]}
          />
        </>
      )}
    </>
  );
};

export default Definition;
