"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Label = ({ label }: { label: any }) => {
  return (
    <>
      {label && (
        <>
          <div className="tooltip_label col-span-1  self-center">
            <h2 className="px-3 font-bold">Label:</h2>
          </div>
          <div className=" col-span-3 ">
            <h2 className="break-all text-lg font-bold ">
              <span
                className=" bg-gradient-to-r from-primary to-info bg-clip-text text-transparent 
               "
              >
                {toSentenceCase(label)}
              </span>
            </h2>
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_label`}
            place="bottom"
            content={tooltip_text["label"]}
          />
        </>
      )}
    </>
  );
};

export default Label;
