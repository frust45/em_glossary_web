"use client";

import { CONTRIBUTORS_ORCID } from "@/helper/const";
import { linked_orcid_to_name } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import Image from "next/image";
import { Tooltip } from "react-tooltip";

const Contributors = ({ contributors }: { contributors: any }) => {
  return (
    <>
      {contributors && (
        <>
          <div className="tooltip_contributors col-span-1 self-center">
            <h2 className="px-3 font-bold">Contributors:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(contributors).map(([key, value]: any) => (
              <span key={`contributors${key}`}>
                <p className=" px-2 text-justify">
                  <Image
                    src="/images/orcid.svg"
                    alt="orcid"
                    width={30}
                    height={30}
                    className="mr-2 inline-flex w-5"
                  />
                  <a
                    href={value}
                    target="_blank"
                    className="tooltip_contributor"
                    rel="noopener noreferrer"
                  >
                    {linked_orcid_to_name(value, CONTRIBUTORS_ORCID)}
                  </a>
                </p>
                <Tooltip
                  className="my_tooltip"
                  anchorSelect={`.tooltip_contributor`}
                  place="bottom"
                  content={`${tooltip_text["contributor"]}`}
                />
              </span>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_contributors`}
            place="bottom"
            content={tooltip_text["contributors"]}
          />
        </>
      )}
    </>
  );
};

export default Contributors;
