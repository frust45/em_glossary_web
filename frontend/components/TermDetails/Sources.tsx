"use client";

import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Sources = ({ sources }: { sources: any }) => {
  return (
    <>
      {sources && (
        <>
          <div className="tooltip_sources col-span-1 self-center">
            <h2 className="px-3 font-bold">Sources:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(sources).map(([key, value]: any) => (
              <p key={`sources${key}`} className="text-justify ">
                <a
                  href={value}
                  target="_blank"
                  className="text-ellipsis"
                  rel="noopener noreferrer"
                >
                  {value}
                </a>
              </p>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_sources`}
            place="bottom"
            content={tooltip_text["sources"]}
          />
        </>
      )}
    </>
  );
};

export default Sources;
