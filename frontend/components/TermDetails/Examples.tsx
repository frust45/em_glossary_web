"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Examples = ({ examples }: { examples: any }) => {
  return (
    <>
      {examples && (
        <>
          <div className="tooltip_examples col-span-1 self-center">
            <h2 className="px-3 font-bold">Examples:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(examples).map(([key, value]: any) => (
              <>
                <h5 key={`examples${key}`} className="font-bold">
                  {toSentenceCase(value)}
                </h5>
              </>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_examples`}
            place="bottom"
            content={tooltip_text["examples"]}
          />
        </>
      )}
    </>
  );
};

export default Examples;
