"use client";

import React, { useEffect, useState } from "react";
import axios from "axios";
import { ColorRing } from "react-loader-spinner";
import { toSentenceCase } from "@/helper/helper";
import Link from "next/link";

const SearchBar = () => {
  const [query, setQuery] = useState("");
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const handleReset = () => {
    setQuery("");
  };
  function handleSearch(e: any) {
    let search = e.target.value;

    if (search.length > 0) {
      setQuery(search);
    }
    if (search.length <= 0) {
      setQuery(search);
    }
    // console.log(search);
  }

  const handleKeyDown = (e: any) => {
    if (query?.length > 0) {
      if (e.key === "Enter") {
        const search = e.target.value;

        // console.log(search);
        setQuery(search);
      }
    } else {
      return null;
    }
  };

  useEffect(() => {
    if (query.length > 0) {
      setLoading(true);
      axios
        .get(process.env.NEXT_PUBLIC_BACKEND_URL + "api/search_terms/" + query)
        .then((data) => {
          // console.log(data.data.result);
          setData(data.data.result);
          setLoading(false);
        });
    }
  }, [query]);

  return (
    <>
      <div className="no-print container mx-auto">
        <div
          className="m-4 flex flex-col rounded-lg 
        border bg-info/[.2] p-2 sm:mx-12"
        >
          <div className="flex justify-center">
            <div className="relative flex w-full sm:w-2/5">
              {!query && (
                <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                  <svg
                    className="h-4 w-4 text-gray-500 "
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 20 20"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                    />
                  </svg>
                </div>
              )}
              <input
                type="search"
                id="default-search"
                value={query}
                className="block w-full rounded-xl border-2 border-primary bg-gray-50  p-4 pl-10  text-sm text-gray-900 "
                placeholder="Search Terms..."
                required
                onKeyDown={handleKeyDown}
                onChange={handleSearch}
              />
              {query && (
                <div
                  className="absolute  inset-y-0 left-0 flex cursor-pointer items-center pl-3"
                  onClick={handleReset}
                >
                  <svg
                    className="h-5 w-5 text-gray-500"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M10 2a8 8 0 100 16 8 8 0 000-16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                      clipRule="evenodd"
                    />
                  </svg>
                </div>
              )}

              {isLoading && (
                <div
                  className="absolute bottom-2.5 right-20 flex cursor-pointer items-center"
                  onClick={handleReset}
                >
                  <ColorRing
                    visible={true}
                    height="40"
                    width="40"
                    ariaLabel="blocks-loading"
                    wrapperStyle={{}}
                    wrapperClass="blocks-wrapper"
                    colors={[
                      "#e15b64",
                      "#f47e60",
                      "#f8b26a",
                      "#abbd81",
                      "#849b87",
                    ]}
                  />
                </div>
              )}

              <button
                type="button"
                className="focus:ring-blue-300 
                 absolute bottom-2.5 right-2 rounded-lg bg-primary px-4 py-2 text-sm font-medium text-white hover:bg-info focus:bg-fucus focus:outline-none focus:ring-4"
              >
                Search
              </button>
            </div>
          </div>

          {query.length > 0 && (
            <div className="z-50 flex justify-center">
              <div className="absolute  h-96 w-full max-w-sm overflow-y-auto rounded-lg border border-gray-200 bg-white p-4 shadow sm:p-6">
                <h5 className="mb-3 text-base font-semibold text-primary md:text-xl">
                  Searched Result
                </h5>
                <p className="text-sm font-normal text-gray-500">
                  In the below you can find related terms during the typing!
                </p>
                {data && data.length > 0 ? (
                  <ul className="my-4 space-y-3 ">
                    {data.map((item: any, index: number) => (
                      <li key={index + item.url_slug} onClick={handleReset}>
                        <Link
                          href={`/term/${item.url_slug}`}
                          className="group flex items-center rounded-lg bg-gray-50 p-3 text-base font-bold text-primary hover:bg-gray-100 hover:shadow"
                        >
                          <span className="ml-3 flex-1 whitespace-nowrap">
                            {toSentenceCase(item.label)}
                          </span>
                        </Link>
                      </li>
                    ))}
                  </ul>
                ) : (
                  <>
                    <h2>No result found for these keywords</h2>
                  </>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default SearchBar;
