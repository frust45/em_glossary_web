// "use client";
//
// import { useState } from "react";
// import { Switch } from "@headlessui/react";
// import { useTermsSourceContext } from "../TermsSourceContextProvider";
//
// const SingleSource = ({ source }: {source:any}) => {
//   const [enabled, setEnabled] = useState(source.active);
//   const { changeSourceStatus } = useTermsSourceContext();
//
//   const changeSource = (slug_name, e) => {
//     console.log(slug_name, e);
//     changeSourceStatus(slug_name, e);
//   };
//   return (
//     <div className="flex flex-row  justify-between">
//       <span>{source.name}</span>
//       <Switch
//         checked={source.active}
//         onChange={(e) => changeSource(source.slug_name, e)}
//         className={`${
//           source.active ? "bg-primary" : "bg-gray-600"
//         } relative inline-flex h-[18px] w-[34px] shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus-visible:ring-2  focus-visible:ring-white/75`}
//       >
//         <span className="sr-only">{source.name}</span>
//
//         <span
//           className={`${
//             source.active ? "translate-x-4" : "translate-x-0"
//           } pointer-events-none inline-block h-[14px] w-[14px] transform rounded-full bg-white shadow-lg ring-0 transition duration-200 ease-in-out`}
//         />
//       </Switch>
//     </div>
//   );
// };
//
// export default SingleSource;
