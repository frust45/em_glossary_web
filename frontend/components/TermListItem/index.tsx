import Link from "next/link";
import { toDefinitionCase, toSentenceCase } from "../../helper/helper";
import TermBar from "./TermBar";
const TermListItem = ({ term }: { term: any }) => {
  return (
    <>
      {term && (
        <Link
          href={`/term/${term.url_slug}`}
          className="m-2 flex  flex-col justify-start   rounded-lg border bg-white shadow-sm hover:bg-gray-100 md:flex-row"
        >
          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <h2 className="break-all p-2 text-lg font-bold  text-gray-900  ">
              <span
                className="bg-gradient-to-r from-primary to-info bg-clip-text text-transparent 
               "
              >
                {toSentenceCase(term.label)}
              </span>
            </h2>

            <div className="flex flex-col flex-wrap">
              <h5 className="px-2 font-normal text-primary ">
                {toDefinitionCase(term.definition)}
              </h5>
            </div>
            <div className="flex justify-end">
              <TermBar term={term} />
            </div>
          </div>
        </Link>
      )}
    </>
  );
};

export default TermListItem;
