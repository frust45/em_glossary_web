"use client";
import { parseISO, format } from "date-fns";

export default function MyDate({ dateString }: any) {
  // console.log(dateString);
  const date1 = parseISO(dateString);
  const date = format(date1, "LLLL d, yyyy");
  return date;
}
