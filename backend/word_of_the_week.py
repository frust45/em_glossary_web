#!/usr/bin/env python3


import os
from datetime import datetime, timedelta, timezone

from dotenv import load_dotenv, find_dotenv
from pymongo import MongoClient, DESCENDING

from terms import get_terms_collection

load_dotenv(find_dotenv())

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")

N_WEEK = 10
UPDATE_ALLOW = True
FILTER_TERM_LIST = [
    "electron beam",
    "electron diffraction",
    "electron diffraction pattern",
    "ion beam",
    "time period",
]


def get_word_of_the_week_collection():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["word_of_the_week"]


def get_word_of_the_week_list():
    collection = get_word_of_the_week_collection()
    result = collection.find()

    data = {
        "result": list(result),
    }
    return data


def get_current_word_of_the_week():
    collection = get_word_of_the_week_collection()
    result = collection.find().sort("date", DESCENDING).limit(1)[0]
    term = get_terms_collection().find_one({"_id": result["term_id"]})

    data = {
        "result": term,
    }
    return data


def save_new_word_of_the_week(term_id):
    collection = get_word_of_the_week_collection()
    result = {}

    term = get_terms_collection().find_one({"_id": term_id})
    # pprint.pprint(term)
    word_of_the_week = {
        "term_id": term_id,
        "label": term["label"],
        "definition": term["definition"],
        "url_slug": term["url_slug"],
        "date": datetime.now(tz=timezone.utc),
    }

    inserted_word_id = collection.insert_one(word_of_the_week).inserted_id
    if inserted_word_id:
        result = collection.find_one(inserted_word_id)

    return result


def _update_word_of_the_week(term_id):
    collection = get_word_of_the_week_collection()
    last_word_of_the_week = collection.find({}).sort("date", DESCENDING).limit(1)[0]
    term = get_terms_collection().find_one({"_id": term_id})
    print(
        f"Last word of the week was: ({last_word_of_the_week['label']}), "
        f"and update to: ({term['label']})"
    )
    word_of_the_week = {
        "term_id": term_id,
        "label": term["label"],
        "definition": term["definition"],
        "url_slug": term["url_slug"],
        "date": datetime.now(tz=timezone.utc),
    }

    return collection.update_one(
        {"_id": last_word_of_the_week["_id"]}, {"$set": word_of_the_week}
    )


def _select_random_term():
    pipeline = [
        {"$sample": {"size": 1}},
    ]
    selected_term = list(get_terms_collection().aggregate(pipeline))[0]
    # pprint.pprint(selected_term)
    return selected_term


def _is_word_exist_for_current_week():
    today = datetime.now()
    last_week = today - timedelta(days=7)

    word_count = get_word_of_the_week_collection().count_documents(
        {"date": {"$gte": last_week}}
    )
    # print(word_count)
    if word_count > 0:
        return True
    else:
        return False


def _count_word_of_the_week_in_n_week(label):
    today = datetime.now()
    date_n_week = today - timedelta(weeks=10)

    word_count = get_word_of_the_week_collection().count_documents(
        {"label": label, "date": {"$gte": date_n_week}}
    )

    return word_count


def _generate_new_word_of_the_week(update):
    selected_term = _select_random_term()
    label = selected_term["label"]
    term_id = selected_term["_id"]
    print(f"The suggested random term: ({label})")
    if label in FILTER_TERM_LIST:
        print("The term is in the filtered list.")
        _generate_new_word_of_the_week(update)
    else:
        print("The term is not in the filtered list.")
        print(
            "Now we will go for next step to check this term should not be in N-LAST word of the week!"
        )

        count_word = _count_word_of_the_week_in_n_week(label)
        print(f"Last N week count: {count_word}")

        if count_word > 0:
            print(f"The {label} term exit on last {N_WEEK} week!")
            _generate_new_word_of_the_week(update)
        else:
            print(f"The {label} term NOT exit on last {N_WEEK} week!")
            if update:
                data = _update_word_of_the_week(term_id)
                print(f"Successfully UPDATE the word of the week")
            else:
                data = save_new_word_of_the_week(term_id)
                print(f"Successfully SAVE the word of the week")
            return data


def generate_new_word_of_the_week():
    word_exit = _is_word_exist_for_current_week()

    if word_exit:
        print(f"Word for the current week exist status {word_exit}")
        print("Word of the week already exist!")

        if UPDATE_ALLOW:
            print("Update option is ON!, so we will update, if word already exist!")
            return _generate_new_word_of_the_week(True)
        print(
            "Update option is OFF!, just exit without effecting the current word of the week!"
        )
        return None
    else:
        print("Word of the week do not exist!, so create it!")
        return _generate_new_word_of_the_week(False)


if __name__ == "__main__":
    generate_new_word_of_the_week()
