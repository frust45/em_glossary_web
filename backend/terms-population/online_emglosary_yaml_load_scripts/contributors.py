contributors = [
    {
        "name": "Markus Wollgarten",
        "orcid": "https://orcid.org/0000-0003-2285-9329",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Markus Kühbach",
        "orcid": "https://orcid.org/0000-0002-7117-5196",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Steffen Brinckmann",
        "orcid": "https://orcid.org/0000-0003-0930-082X",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Rossella Aversa",
        "orcid": "https://orcid.org/0000-0003-2534-0063",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Robert Wendt",
        "orcid": "https://orcid.org/0000-0001-8480-4775",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Reetu Elza Joseph",
        "orcid": "https://orcid.org/0000-0002-1507-9327",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Luigia Cristiano",
        "orcid": "https://orcid.org/0000-0002-1418-0984",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Adrien Teurtrie",
        "orcid": "https://orcid.org/0000-0002-6004-2304",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Clemens Mangler",
        "orcid": "https://orcid.org/0000-0001-5870-4658",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Christoph Pauly",
        "orcid": "https://orcid.org/0000-0002-6368-2067",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Ashish Suri",
        "orcid": "https://orcid.org/0000-0003-4745-9735",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Cecile Hebert",
        "orcid": "https://orcid.org/0000-0002-7086-1901",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Helmut Kohl",
        "orcid": "https://orcid.org/0000-0001-6534-4195",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Annika Strupp",
        "orcid": "https://orcid.org/0000-0002-0070-4337",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Peter Konijnenberg",
        "orcid": "https://orcid.org/0000-0002-1278-4928",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Sandor Brockhauser",
        "orcid": "https://orcid.org/0000-0002-9700-4803",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Rasmus R. Schroeder",
        "orcid": "https://orcid.org/0009-0008-6410-7217",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Sebastian Slawik",
        "orcid": "https://orcid.org/0000-0002-3054-8734",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Alexander Clausen",
        "orcid": "https://orcid.org/0000-0002-9555-7455",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Said Fathalla",
        "orcid": "https://orcid.org/0000-0002-2818-5890",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Simeon Ehrig",
        "orcid": "https://orcid.org/0000-0002-8218-3116",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "scientist_staff",
    },
    {
        "name": "Oonagh Mannix",
        "center": "HZB",
        "orcid": "https://orcid.org/0000-0003-0575-2853",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "Oonagh_Mannix.jpg",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Volker Hofmann",
        "orcid": "https://orcid.org/0000-0002-5149-603X",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "VHoffmann.jpg",
        "center": "Forschungszentrum Jülich",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Pedro Videgain Barranco",
        "orcid": "https://orcid.org/0000-0003-0000-4784",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Özlem Özkan",
        "center": "HZB",
        "orcid": "https://orcid.org/0000-0003-1965-7996",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "Oeslem-Oeskar.jpg",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Ahmad Zainul Ihsan",
        "orcid": "https://orcid.org/0000-0002-1008-4530",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Unknown",
        "staff_type": "HMC_staff",
    },
    {
        "name": "Mojeeb Rahman Sedeqi",
        "center": "HZB",
        "orcid": "https://orcid.org/0000-0002-9694-0122",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "Mojeeb_Rahman_Sedeqi.jpg",
        "staff_type": "HMC_staff",
    },
]
