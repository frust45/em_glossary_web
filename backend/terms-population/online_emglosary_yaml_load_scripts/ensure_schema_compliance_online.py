#!/usr/bin/env python3

import yaml
import json
from jsonschema import Draft7Validator, ValidationError
from pathlib import Path
import os
from dotenv import load_dotenv, find_dotenv


load_dotenv(find_dotenv())


# SCRIPT_DIR = Path().resolve()
# SCHEMA_FILE = SCRIPT_DIR / "../schema/termSchema.json"


def are_terms_valid(yaml_dir, schema):
    has_error = False
    with open(schema, "r") as schema_file:
        termSchema = json.load(schema_file)  # load json-schema
        validator = Draft7Validator(termSchema)

        # Traverse the directory tree and execute Git commands
        for root, dirs, files in os.walk(yaml_dir):
            for file in files:
                if file.endswith(".yaml"):
                    path = os.path.join(root, file)
                    with open(path, "r") as file:
                        try:
                            term = list(yaml.safe_load_all(file))  # load term
                            try:
                                validator.validate(term[0])  # validate term

                            except ValidationError as e:
                                print(f"Term definition is not valid: {file}.\n{e}\n\n")
                                has_error = True

                        except yaml.YAMLError as e:
                            print(f"Could not load YAML file: {file}\n\n")
                            has_error = True
    return has_error != True
